package com.hyh.simple;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Summerday
 */
@Service
@Slf4j
public class SimpleUserService {

    @Autowired
    SimpleEmailService emailService;

    @Autowired
    SimpleCouponService couponService;


    public void register(String username){

        log.info("用户 [{}] 注册成功", username);
    }

}
