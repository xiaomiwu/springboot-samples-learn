package com.hyh.simple;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author Summerday
 */

@Service
@Slf4j
public class SimpleEmailService {

    public void sendEmail(String username) {
        Thread thread = new Thread(()->{
            try {
                // 模拟发邮件耗时操作
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("给用户 [{}] 发送邮件...", username);
        });
        thread.start();
    }
}
