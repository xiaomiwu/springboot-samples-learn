<#macro greet>
    <font size="+2">Summerday</font>
</#macro>

<#macro greet_2 person>
    <font size="+2">Hello ${person}!</font>
</#macro>

<#macro nest_test>
    <#nested >
</#macro>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
        <link rel="stylesheet" href="/me.css">
    </head>
    <body>

            <div class="top">
                <p>
                    <#--使用内建函数size-->
                    users size = ${users?size}
                </p>
                <#list users>
                <p>Users:
                <ul>
                    <#items as user>
                    <li>${user.id}--${user.username}<br>
                        </#items>
                </ul>
                <#else>
                <p>no users!
                    </#list>


                <div style="height: 1500px">
                    <#--宏调用-->
                    <@greet></@greet>
                    <hr>
                    <#--带参数宏调用-->
                    <@greet_2 person="天乔巴夏"/>
                    <hr>
                    <@nest_test>
                        hyh
                    </@nest_test>
                    <br>
                    <@strstr username="hyh" city="杭州">
                        ${result}
                    </@strstr>
                    <br>
                    <span>${timeAgo(date)}</span>
                </div>

            </div>
            <#include "footer.ftl"/>


    </body>
</html>

