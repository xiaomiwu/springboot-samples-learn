package com.hyh.freemarker.controller;

import com.hyh.freemarker.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Summerday
 */

@Controller
public class TestController {

    @GetMapping("test")
    public String test(HttpServletRequest request){
        List<User> users = new LinkedList<>();
        for(int i = 0; i < 10; i ++){
            User user = new User();
            user.setId(i);
            user.setUsername("name = " + i);
            users.add(user);
        }
        request.setAttribute("users",users);
        request.setAttribute("date",new Date());
        return "test";
    }
}
