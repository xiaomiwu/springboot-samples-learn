package com.hyh.shiro.common;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Summerday
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public AjaxResult handle(HttpServletRequest request, Throwable ex) {
        if(ex instanceof AuthorizationException){
            log.info("authorization ex");
        }
        log.error("异常:url:{},ex:{}", request.getRequestURL(), ex.getMessage());
        return AjaxResult.error(HttpStatus.UNAUTHORIZED.toString(),ex.getMessage());
    }
}
