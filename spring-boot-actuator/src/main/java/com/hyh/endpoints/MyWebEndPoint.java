package com.hyh.endpoints;

import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.web.annotation.RestControllerEndpoint;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Summerday
 */

@Component
@RestControllerEndpoint(id = "web")
public class MyWebEndPoint {

    Runtime runtime = Runtime.getRuntime();

    @GetMapping("/")
    public Map<String, Object> getData() {
        Map<String, Object> map = new HashMap<>();
        map.put("available-processors", runtime.availableProcessors());
        map.put("free-memory", runtime.freeMemory());
        map.put("max-memory", runtime.maxMemory());
        map.put("total-memory", runtime.totalMemory());
        return map;
    }

}
