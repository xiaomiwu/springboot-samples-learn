package com.hyh.primer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * 自定义baseMapper接口
 * @author Summerday
 */
public interface MyBaseMapper<T> extends BaseMapper<T> {

    //自定义方法,删除所有,返回影响行数
    int deleteAll();

    // 根据id找到
    T findOne(Serializable id);

    /**
     * 以下定义的 4个 method 其中 3 个是内置的选装件
     */
    int insertBatchSomeColumn(List<T> entityList);

    int alwaysUpdateSomeColumnById(@Param(Constants.ENTITY) T entity);

    int deleteByIdWithFill(T entity);
}
