package com.hyh.exhand.exception;

/**
 * @author Summerday
 */
public class YourException extends BaseException {


    public YourException(String msg, int code) {
        super(msg, code);
    }
}
