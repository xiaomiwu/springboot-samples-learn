package com.hyh.exhand.controller;

import com.hyh.exhand.exception.YourException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Summerday
 */

@RestController
public class MyController {

    /**
     * 模拟用户数据访问
     */
    @GetMapping("/test")
    public String testMyEx() throws Exception {
        double alpha = 0.9;
        if(Math.random() < alpha) throw new YourException("错误!", 500);
        return "success";
    }
}
