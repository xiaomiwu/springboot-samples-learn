package com.hyh;

import com.hyh.dao.BlogDao;
import com.hyh.dao.UserDao;
import com.hyh.entity.Blog;
import com.hyh.entity.User;
import com.hyh.specs.UserSpecs;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@SpringBootTest
class SpringBootJpaApplicationTests {

    @Resource
    UserDao userDao;

    @Resource
    BlogDao blogDao;

    @Test
    void testJPA() {
        User user = userDao.save(new User(null, "summerday", "123456", "hangzhou"));
        System.out.println("添加用户: " + user);
        User u = userDao.findByUsernameAndPassword("summerday", "123456");
        System.out.println("根据用户名和密码查询用户: " + u);
        long count = userDao.count();
        System.out.println("当前用户数量: " + count);
        PageRequest page = PageRequest.of(0, 5, Sort.by(Sort.Order.desc("id")));
        Page<User> all = userDao.findAll(page);
        System.out.println("分页 + 根据id逆序 查询结果: " + all.getContent());
        if (userDao.existsById(u.getId())) {
            userDao.deleteById(u.getId());
            System.out.println("删除id为" + u.getId() + "的用户成功");
        }
        long c = userDao.count();
        System.out.println("剩余用户数为: " + c);
    }

    @Test
    public void testSpecs() {
        userDao.save(new User(null, "summerday", "123456", "hangzhou"));
        userDao.save(new User(null, "summy", "1256", "Beijing"));
        userDao.save(new User(null, "summerdy", "3456", "HongKong"));
        userDao.save(new User(null, "summerda", "1256", "Shanghai"));

        Specification<User> specification = UserSpecs.contains("username", "summer");
        List<User> all = userDao.findAll(specification);
        System.out.println(all);
    }

    @Test
    public void testAddBlog() {
        Blog blog = new Blog();
        blog.setAuthor("summerday");
        blog.setId(null);
        blog.setName("Java学习笔记");
        blog.setPublishTime(Date.from(Instant.now().minus(Duration.ofDays(2))));
        blog.setUpdateTime(new Date());
        blogDao.save(blog);
    }

    @Test
    public void findByExample() {
        Specification<Blog> specification = new Specification<Blog>() {
            @Override
            public Predicate toPredicate(Root<Blog> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(cb.equal(root.get("status"), 1)); // 查询status状态为1
                predicates.add(cb.like(root.get("author"), "%summer%")); // author名称中存在summer
                predicates.add(cb.between(root.get("publishTime"),  // 发布时间7天前至今
                        Date.from(Instant.now().minus(Duration.ofDays(7))), new Date()));

                return cb.and(predicates.toArray(new Predicate[0]));
            }
        };
        // 按照条件查询,并按照publish_time升序
        List<Blog> blogs
                = blogDao.findAll(specification, Sort.by(Sort.Direction.ASC, "publishTime"));
        blogs.forEach(System.out::println);
    }
}
