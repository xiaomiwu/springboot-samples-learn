package com.hyh.config;

import com.sun.scenario.effect.impl.prism.PrImage;
import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Summerday
 */

@ConfigurationProperties(prefix = "my")
@Data
@ToString
public class MyProperties {

    private String secret;
    private Integer number;
    private Long bignumber;
    private String uuid;
    private Long lessThanTen;
    private Long inRange;

}
