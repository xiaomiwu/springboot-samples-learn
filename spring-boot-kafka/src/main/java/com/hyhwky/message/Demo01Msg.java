package com.hyhwky.message;

import lombok.Data;

/**
 * @author Summerday
 */
@Data
public class Demo01Msg {

    public static final String TOPIC = "DEMO_01";

    private Integer id;
}
